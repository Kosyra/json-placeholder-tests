package apiTests;

import apiTests.utils.Constants;
import io.restassured.RestAssured;
import org.junit.Before;
//import org.testng.annotations.BeforeTest;

public class BaseApiTest {
    @Before
    public void SetUp(){
        RestAssured.baseURI = Constants.URL;
    }
}
